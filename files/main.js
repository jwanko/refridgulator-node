var socket = io.connect(); 

function sendAttribute(id, x, y) {
	socket.emit('getCoordinates', {id: $id, x: $x, y: $y});
}
function addRotation(item){
	$rot = (Math.random()*6) - 3 ;
	item.css({
	    "transform": "rotate(" + $rot +"deg)",
	    "-ms-transform": "rotate(" + $rot +"deg)",
	    "-webkit-transform": "rotate(" + $rot +"deg)",
});
}
$(document).ready(function(){

    socket.on('sendCoordinates', function (data) {
        if ($('#' + data.id).hasClass('ui-draggable-dragging')) {} else {
            addRotation($('#' + data.id));
            $('#' + data.id).animate({left: data.x + "%", top: data.y + "%"});

        }
    });
    
    $drag = $('.drag');
    
    socket.on('message', function (data) {
        $drag.each(function(){
            $data = data[$(this).attr('id')];
            if ($data) {
                $('#' + $data.id).animate({left: $data.x + "%", top: $data.y + "%"}, 1000);
            } else {
                $('#' + $(this).attr('id')).animate({left: (Math.random() * 95) + "%", top: (Math.random() * 95) + "%"}, 1000);
            }
        });
        $('.overlay').delay(1500).fadeOut(1500);
    });
	
	$drag.each(function(){
	   addRotation($(this));
	});

	$drag.draggable({ containment: "#container", scroll: false });
	$drag.bind("dragstart", function(event, ui) {
	    $(this).css('box-shadow', '1px 1px 3px rgba(0,0,0,0.4)');
	});
	$drag.bind( "dragstop", function(event, ui) {
		addRotation($(this));
		$(this).css('box-shadow','none');
		$id = $(this).attr('id');
		$x = (ui.position.left / $("#container").width()) * 100,
		$y = (ui.position.top / $("#container").height()) * 100;
		sendAttribute($id, $x, $y);
		$('#' + $id).css({left: $x + "%", top: $y + "%"});
		ga('send', 'event', 'magnet', 'move', $id);
	});
});
