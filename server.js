var app = require('express')(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server),
    port = 8080,
    url  = 'http://localhost:' + port + '/';
if(process.env.SUBDOMAIN){
  url = 'http://' + process.env.SUBDOMAIN + '.jit.su/';
}

server.listen(port);

//Get Files
app.get('/', function (req, res) {
  res.sendfile(__dirname + '/index.html');
});
app.get('/main.js', function (req, res) {
  res.sendfile(__dirname + '/files/main.js');
});

app.get('/jquery.ui.touch-punch.min.js', function (req, res) {
  res.sendfile(__dirname + '/files/jquery.ui.touch-punch.min.js');
});

app.get('/jquery-1.10.1.min.js', function (req, res) {
  res.sendfile(__dirname + '/files/jquery-1.10.1.min.js');
});

app.get('/normalize.min.css', function (req, res) {
  res.sendfile(__dirname + '/files/normalize.min.css');
});

app.get('/main.css', function (req, res) {
  res.sendfile(__dirname + '/files/main.css');
});

app.get('/modernizr-2.6.2-respond-1.1.0.min.js', function (req, res) {
  res.sendfile(__dirname + '/files/modernizr-2.6.2-respond-1.1.0.min.js');
});

app.get('/overlay.gif', function (req, res) {
  res.sendfile(__dirname + '/files/overlay.gif');
});

app.get('/fridge.png', function (req, res) {
  res.sendfile(__dirname + '/files/fridge.png');
});

app.get('/bss_logo.jpg', function (req, res) {
  res.sendfile(__dirname + '/files/bss_logo.jpg');
});

app.get('/fb.png', function (req, res) {
  res.sendfile(__dirname + '/files/fb.png');
});

//End Get Files

function moveRoom(id, fromRoom, toRoom) {
  console.log("moving "+id+" from "+fromRoom+" to "+toRoom);
  $user++;
  io.sockets.sockets[id].leave(fromRoom).join(toRoom);
}

var $user = 0;
var $room = 1;
var $i = 1;
var blockLocs = new Object;
while ($i <= 200) {
    blockLocs['box' + $i] = {id: "box" + $i, x: Math.random() * 95, y: Math.random() * 95};
    $i++;
}

//Socket.io emits this event when a connection is made.
io.sockets.on('connection', function (socket) {
  $user++;
  if ($user % 20 == 19) {
    $room++;
  }
  roomNum = "room"+$room;
  socket.set('roomNumber', roomNum);
  socket.join(roomNum);
  socket.emit('message', blockLocs);
  
  socket.on('getCoordinates', function (data) {
    roomNum = socket.get('roomNumber', function (err, name) {
      socket.broadcast.to(name).emit('sendCoordinates', data);
    });
    blockLocs[data.id] = data;
  });

  socket.on('disconnect', function (socket) {
    io.sockets.emit('user disconnected');
  });
});

setInterval(function(){
  var $rooms = io.sockets.manager.rooms;
  for (var i in $rooms){
    if ($rooms[i].length == 1) {
      if(i != "/room"+$room) {
        id = $rooms[i][0];
        moveRoom(id, i.substr(1), "room"+$room);
      }
    }
  }
}, 10000);